var chainlist = [
    {
        accountMode: 'permissionedWithCert',
        chainId: 'chainmaker_testnet_chain',
        chainName: '开放测试网络 (Cert)',
        description: '长安链开放测试网络（Cert）是长安链官方推出的账户模式为证书账户的测试网络，供社区用户进行区块链应用开发测试。',
        hostName: 'chainmaker.org',
        logo: 'https://git.chainmaker.org.cn/chainmaker/issue/-/raw/master/images/logo%E8%8B%B1%E6%96%87.png?inline=false',
        rpcs: [
            {
                url: 'certnode1.chainmaker.org.cn:13301',
                orgName: "长安链官方"
            },
            {
                url: 'certnode1.chainmaker.org.cn:13302',
                orgName: "长安链官方"
            },
            {
                url: 'certnode1.chainmaker.org.cn:13303',
                orgName: "长安链官方"
            },
            {
                url: 'certnode1.chainmaker.org.cn:13304',
                orgName: "长安链官方"
            }
        ],
        tlsEnable: true,
        browserLink: 'https://explorer-testnet.chainmaker.org.cn/',
    },
    {
        accountMode: 'public',
        chainId: 'chainmaker_testnet_pk',
        chainName: '开放测试网络 (Public)',
        description: '长安链开放测试网络（Public）是长安链官方推出的与长安链开放联盟链主链相对应的，账户模式为公钥账户的测试网络，供社区用户进行区块链应用开发测试。',
        hostName: 'chainmaker.org',
	protocol: 'HTTP',
        logo: 'https://git.chainmaker.org.cn/chainmaker/issue/-/raw/master/images/logo%E8%8B%B1%E6%96%87.png?inline=false',
        rpcs: [
            {
                url: 'testnode.chainmakernet.com:17301',
                orgName: "长安链官方"
            },
            {
                url: 'testnode.chainmakernet.com:17302',
                orgName: "长安链官方"
            },
            {
                url: 'testnode.chainmakernet.com:17303',
                orgName: "长安链官方"
            },
            {
                url: 'testnode.chainmakernet.com:17304',
                orgName: "长安链官方"
            }
        ],
        browserLink: 'http://scan-testnetpk.chainmakernet.com/',
    },
    {
        accountMode: 'permissionedWithCert',
        chainId: 'chain_dcwcu',
        chainName: '厦门市公证处长安链',
        description: '厦门市公证处存证及相关公证业务的区块链技术实现，账户模式为证书账户。',
        hostName: 'consensus1-org69vkhk0.chainmaker-4r1p6g4wt5',
        logo: 'https://fxyundns.fxyun.xyz/enc/img/edca169dff054430958b9c7957ed87f5',
        tlsEnable:true,
        rpcs: [
            {
                url: 'org69vkhk0-chainmaker-4r1p6g4wt5.tbaas.tech:8080',
                orgName: "厦门市公证处"
            }
        ],
        browserLink: 'https://chainmaker.fxyun.xyz/',
    }, {
        accountMode: 'public',
        chainId: 'chain1',
        chainName: 'ZTimeChain',
        description: 'ZtimeChain',
        hostName: 'chainmaker.org',
        // logo: 'https://fxyundns.fxyun.xyz/enc/img/edca169dff054430958b9c7957ed87f5',
        tlsEnable:false,
        rpcs: [
            {
                url: '111.9.175.254:12301',
            },
            {
                url: '111.9.175.254:12302',
            },
            {
                url: '111.9.175.254:12303',
            },
            {
                url: '111.9.175.254:12304',
            },
        ],
        // browserLink: 'https://chainmaker.fxyun.xyz/',
    },


    

];
